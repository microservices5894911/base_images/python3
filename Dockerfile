FROM python:3.9.16-alpine3.17

LABEL url="https://gitlab.com/microservices5894911/base_images/python3"

RUN apk --no-cache add libcurl zlib \
 && apk --no-cache --virtual devdeps add curl musl-dev gcc \
 && pip3 install --upgrade pip setuptools wheel \
 && pip3 install yarl \
 && pip3 freeze > /tmp/installed-packages.txt \
 && apk del --purge devdeps
