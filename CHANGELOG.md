## Python 3.9.16-? (2023-03-17)
- TODO

### Software versions
- alpine-baselayout 3.4.0-r0
- alpine-baselayout-data 3.4.0-r0
- alpine-keys 2.4-r1
- apk-tools 2.12.10-r1
- brotli-libs 1.0.9-r9
- busybox 1.35.0-r29
- busybox-binsh 1.35.0-r29
- ca-certificates 20220614-r4
- ca-certificates-bundle 20220614-r4
- gdbm 1.23-r0
- keyutils-libs 1.6.3-r1
- krb5-conf 1.0-r2
- krb5-libs 1.20.1-r0
- libbz2 1.0.8-r4
- libc-utils 0.7.2-r3
- libcom_err 1.46.6-r0
- libcrypto3 3.0.8-r0
- libcurl 7.88.1-r0
- libexpat 2.5.0-r0
- libffi 3.4.4-r0
- libintl 0.21.1-r1
- libnsl 2.0.0-r0
- libpq 15.2-r0
- libssl3 3.0.8-r0
- libtirpc 1.3.3-r0
- libtirpc-conf 1.3.3-r0
- libuuid 2.38.1-r1
- libverto 0.3.2-r1
- musl 1.2.3-r4
- musl-utils 1.2.3-r4
- ncurses-libs 6.3_p20221119-r0
- ncurses-terminfo-base 6.3_p20221119-r0
- nghttp2-libs 1.51.0-r0
- readline 8.2.0-r0
- scanelf 1.3.5-r1
- sqlite-libs 3.40.1-r0
- ssl_client 1.35.0-r29
- tzdata 2022f-r1
- xz-libs 5.2.9-r0
- zlib 1.2.13-r0

### Python packages
- cffi 1.15.1
- cryptography 38.0.3
- fastavro 1.7.3
- hiredis 2.2.2
- idna 3.4
- multidict 6.0.4
- psycopg-binary 3.1.4
- psycopg-pool 3.1.6
- psycopg2-binary 2.9.5
- psycopg2 2.9.5
- psycopg 3.1.4
- pycparser 2.21
- typing\_extensions 4.5.0
- u-msgpack-python 2.7.2
- yarl 1.8.2

